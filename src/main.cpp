#include <QString>
#include <QLocale>
#include <QCoreApplication>
#include <QDateTime>
#include <QFont>
#include <QDir>
#include <QFileInfo>
#include <QDebug>
#include <iostream>
#include "lib/gpf_engine.h"
#include "lib/gpf_error.h"

using namespace std;

void display_help(ostream & out) {
	out << "Usage: gpf [OPTION] [FILE]" << endl;
	out << "Searched Glycopeptide on HCD spectra by comparing MS/MS." << endl;
	out << endl;
	out << "Options:" << endl;
	out << "  -h, --help\t\t\tdisplay this help and exit" << endl;
	out << endl;
	out << endl;
	out << "  -v, --version\t\t\tdisplay version number and exit" << endl;
	//  out << "\n" << endl;
	out << endl;
	out << "For additional information, see the gpf Homepage :" << endl;
	out << "http://pappso.inra.fr/bioinfo/gpf/" << endl;
}

void windaube_exit() {
#  if (WIN32)
	cout << "Press any key then enter to exit" << endl;
	getchar();
#endif
}

int main(int argc, char **argv) {

	//QTextStream consoleErr(stderr);
	//QTextStream consoleOut(stdout);

	QCoreApplication app(argc, argv);
	QLocale::setDefault(QLocale::system());
	const QDateTime dt_begin = QDateTime::currentDateTime();
	const QString masschroq_dir_path(QCoreApplication::applicationDirPath());

	QStringList arguments = app.arguments();
	QString fileName;
	/// by default, temporary files go in the temporary directory of the system
	//  QString tmpDirName = QDir::tempPath();

	/// by default, temporary files go in the current working directory
	QString tmpDirName = QDir::currentPath();

	int args_number = arguments.size();
	QString first, option;

	switch (args_number) {

	case 2:
		first = arguments.at(1);
		if ((first == "--help") || (first == "-h")) {
			display_help(cout);
			windaube_exit();
			return 0;
		} else if ((first == "--version") || (first == "-v")) {
			cout << "This is GlycoPeptideFinder version " << GPF_VERSION << ","
					<< endl;
			//GPF_VERSION
			windaube_exit();
			return 0;
		} else
			fileName = first;
		break;
	case 1:
		cerr << "gpf : missing input filename." << endl;
		cerr << "Try 'gpf --help' for more information." << endl;
		windaube_exit();
		return 1;
		break;
	default:
		cerr << "gpf : invalid use." << endl;
		cerr << "Try 'gpf --help' for more information." << endl;
		windaube_exit();
		return 1;
	}

	try
	{
	QFileInfo inputFile(fileName);
	GpfEngine engine;
	engine.readGpfXmlFile(inputFile);
	}
	catch(gpfError &error)
	{
		cerr << "Oups, gpf has failed :" << endl;
		cerr << error.qwhat().toStdString() << endl;

		windaube_exit();
		return 1;
	}

	windaube_exit();
	return 0;
}

