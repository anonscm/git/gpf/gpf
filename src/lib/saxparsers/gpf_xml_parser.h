/*
 * gpf_xml_parser.h
 *
 *  Created on: 20 avr. 2012
 *      Author: valot
 */

#ifndef GPF_XML_PARSER_H_
#define GPF_XML_PARSER_H_

#include <QXmlDefaultHandler>
#include <QTextStream>
#include <map>

#include "../spectrum/reader/spectrum_reader_collection.h"
#include "../gpf_params.h"
#include "../spectrum/ms_run.h"


using namespace std;

class GpfXmlParser: public QXmlDefaultHandler {
public:
	GpfXmlParser();
	virtual ~GpfXmlParser();

	bool startElement(const QString & namespaceURI, const QString & localName,
			const QString & qName, const QXmlAttributes & attributes);

	bool endElement(const QString & namespaceURI, const QString & localName,
			const QString & qName);

	bool startDocument();

	bool endDocument();

	bool characters(const QString &str);

	bool fatalError(const QXmlParseException &exception);

	QString errorString() const;

private:

	bool loadCollection();

	bool performedFitting(const QXmlAttributes & attributes);

	/// current parsed text
	QString _currentText;

	/// error message during parsing
	QString _errorStr;

	//current gpf_params
	GpfParams * _current_params;

	//current collection
	SpectrumReaderCollection * _current_collection;

	//Map to params
	map<QString, GpfParams *> _params_ids;

	//map to msrun
	map<QString, MsRun *> _msrun_ids;

	//map to collection
	map<QString, SpectrumReaderCollection *> _collection_ids;

	//TextStream monitor
	QTextStream stream;

};

#endif /* GPF_XML_PARSER_H_ */
