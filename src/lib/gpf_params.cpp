/*
 * gpf_params.cpp
 *
 *  Created on: 11 avr. 2012
 *      Author: valot
 */

#include "gpf_params.h"

GpfParams::GpfParams() {
	_n_most_intense = 50;
	_ppm_precision = 10;
	_min_number_of_peak_to_score = 5;
    _mz_min = 200;
    _percentIntMIn = 0.01;
    _min_cosinus = 0.25;
    _max_pvalue = 0.01;
}

GpfParams::~GpfParams() {
	// TODO Auto-generated destructor stub
}

