/*
 * gpf_xml_parser.cpp
 *
 *  Created on: 20 avr. 2012
 *      Author: valot
 */

#include "gpf_xml_parser.h"
#include "../spectrum/reader/spectrum_reader_fit.h"
#include "../monitor/monitor_fit_result.h"
#include <cstdio>

GpfXmlParser::GpfXmlParser() :
	stream(stdout)
{
	_current_params = 0;
	_current_collection = 0;
}

GpfXmlParser::~GpfXmlParser() {
	//destruct object on map
	std::map<QString, GpfParams *>::iterator it_param;
	std::map<QString, MsRun *>::iterator it_msrun;
	std::map<QString, SpectrumReaderCollection *>::iterator it_collec;

	for(it_param=_params_ids.begin();it_param!=_params_ids.end();it_param++){
		delete(it_param->second);
	}
	for(it_collec=_collection_ids.begin();it_collec!=_collection_ids.end();it_collec++){
		delete(it_collec->second);
	}
	for(it_msrun=_msrun_ids.begin();it_msrun!=_msrun_ids.end();it_msrun++){
		delete(it_msrun->second);
	}
}

bool GpfXmlParser::startElement(const QString & namespaceURI,
		const QString & localName, const QString & qName,
		const QXmlAttributes & attributes) {
	//params
	if (qName == "params") {
		_current_params = new GpfParams();
		_params_ids.insert(
				pair<QString, GpfParams *>(attributes.value("id"),
						_current_params));
	}
	else if(qName == "n_most_intense"){
		_current_params->setMostIntense(attributes.value("value").toInt());
	}
	else if(qName == "mz_min"){
		_current_params->setMzMin(attributes.value("value").toDouble());
	}
	else if(qName == "percentIntMIn"){
		_current_params->setPercentIntMIn(attributes.value("value").toFloat());
	}
	else if(qName == "min_number_of_peak_to_score"){
		_current_params->setMinNumberOfPeakToScore(attributes.value("value").toInt());
	}
	else if(qName == "min_cosinus"){
		_current_params->setMinCosinus(attributes.value("value").toDouble());
	}
	else if(qName == "max_pvalue"){
		_current_params->setMaxPvalue(attributes.value("value").toDouble());
	}
	else if(qName == "ppm_precision"){
		_current_params->setPpmPrecision(attributes.value("value").toDouble());
	}
	//raw data
	else if(qName == "data_file"){
		MsRun * p_msrun = new MsRun(QFileInfo(attributes.value("path")));
		_msrun_ids.insert(pair<QString, MsRun *>(attributes.value("id"),
				p_msrun));
	}

	//collection
	else if(qName == "spectrum_collection"){
		_current_collection = new SpectrumReaderCollection(*_current_params);
		_collection_ids.insert(pair<QString, SpectrumReaderCollection *>(attributes.value("id"),
				_current_collection));
	}
	else if(qName == "spectrum_ref"){
		MsRun * msrun = _msrun_ids.at(attributes.value("msrun"));
		SpectrumId temp (attributes.value("scan").toInt(), msrun);
		temp.setSpectrumInfo(attributes.value("info"));
		_current_collection->addSpectrumId(temp);
	}

	//fitting
	else if(qName == "fitting"){
		this->performedFitting(attributes);
	}
	_currentText = "";
	return true;
}

bool GpfXmlParser::endElement(const QString & namespaceURI,
		const QString & localName, const QString & qName) {
	//start collection loading
	if(qName == "spectrum_collection"){
		return(this->loadCollection());
	}
	return true;
}

bool GpfXmlParser::endDocument() {
	return true;
}

bool GpfXmlParser::startDocument() {
	return true;
}

bool GpfXmlParser::characters(const QString &str) {
	_currentText += str;
	return true;
}

bool GpfXmlParser::fatalError(const QXmlParseException &exception) {
	_errorStr = QObject::tr("Parse error at line %1, column %2:\n"
			"%3").arg(exception.lineNumber()).arg(exception.columnNumber()).arg(
			exception.message());
	return false;
}

QString GpfXmlParser::errorString() const {
	return _errorStr;
}

bool GpfXmlParser::loadCollection(){
	qDebug()<< "Starting collection of spectrum reference";
	std::map<QString, MsRun *>::iterator it_msrun;
	for(it_msrun=_msrun_ids.begin();it_msrun!=_msrun_ids.end();it_msrun++){
		(it_msrun->second)->readSpectrum(*_current_collection);
	}
	qDebug()<< "Finish collection of spectrum reference";
	return true;

}

bool GpfXmlParser::performedFitting(const QXmlAttributes & attributes){
	qDebug()<< "Starting fitting to spectrum reference";
	//initialise new Monitor and reader
	SpectrumReaderFit reader_fit(*_params_ids.at(attributes.value("with_params")),*_current_collection);
	MonitorFitResult * monitor_result = new MonitorFitResult(stream);
	reader_fit.setMonitor(monitor_result);

	//MsRun ids to analysed
	QString msrun_ids = attributes.value("ms_run_ids");
	QStringList list_msrun_ids = msrun_ids.split(" ", QString::SkipEmptyParts);
	QStringList::const_iterator it_msrun;
	for(it_msrun=list_msrun_ids.begin();it_msrun!=list_msrun_ids.end();it_msrun++){
		_msrun_ids.at(*it_msrun)->readSpectrum(reader_fit);
	}
	stream.flush();
	delete(monitor_result);
	qDebug()<< "Finish fitting to spectrum reference";
	return (true);
}
