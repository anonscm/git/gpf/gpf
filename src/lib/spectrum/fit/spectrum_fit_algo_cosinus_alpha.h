/*
 * spectrum_fit_algo_cosinus_alpha.h
 *
 *  Created on: 17 avr. 2012
 *      Author: valot
 */

#ifndef SPECTRUM_FIT_ALGO_COSINUS_ALPHA_H_
#define SPECTRUM_FIT_ALGO_COSINUS_ALPHA_H_

#include "spectrum_fit_algo.h"

class SpectrumFitAlgoCosinusAlpha: public SpectrumFitAlgo {
public:
	SpectrumFitAlgoCosinusAlpha(const GpfParams &gpf_params);
	virtual ~SpectrumFitAlgoCosinusAlpha();
	void compareSpectrum2Spectrum(const Spectrum & ref,
			const Spectrum & toCompare, SpectrumFitResult &toFill) const;

private:
	mcq_double calculated_inner_product(
			std::vector<pair <mcq_double, mcq_double> > &matched_int) const;
	mcq_double calculated_length_vector(
			const Spectrum &spectrum) const;
	const int _min_number_of_peak_to_score;
	const mcq_double _min_cosinus_limit;
};

#endif /* SPECTRUM_FIT_ALGO_COSINUS_ALPHA_H_ */
