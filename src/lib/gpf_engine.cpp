/*
 * gpf_engine.cpp
 *
 *  Created on: 10 avr. 2012
 *      Author: valot
 */

#include <QDebug>
#include <QFileInfo>
#include <QXmlSimpleReader>
#include <QXmlInputSource>
#include <QUrl>
#include <QXmlSchemaValidator>
#include <QXmlSchema>

#include "gpf_engine.h"
#include "gpf_error.h"
#include "saxparsers/gpf_xml_parser.h"
#include "messageHandler.h"
#include "../config.h"

GpfEngine::GpfEngine() {
}

GpfEngine::~GpfEngine() {
}

//void GpfEngine::readMzXMLSpectrum(const QFileInfo &fileinfo) {
//
//	MsRun * msrun = new MsRun(fileinfo);
//	SpectrumReaderCollection refCollection(_gpf_params);
//	SpectrumId id1(3262, msrun);
//	SpectrumId id2(3059, msrun);
//	refCollection.addSpectrumId(id1);
//	refCollection.addSpectrumId(id2);
//
//	msrun->readSpectrum(refCollection);
//
//	SpectrumReaderFit reader_fit(_gpf_params, refCollection);
//
//	//ajout d'un monitor
//	QFile output("/tmp/gpf_result.txt");
//	//initialise Stream
//	QTextStream stream;
////	qDebug() << "Writing fitting result to STDOUT: '";
//	if (output.open(QIODevice::WriteOnly)) {
//		stream.setDevice(&output);
//	} else {
//		throw gpfError(QObject::tr("cannot open the fittinq output file \n"));
//	}
//	qDebug() << "Writing fitting result to : '" << output.fileName();
//
//	MonitorFitResult * monitor_result = new MonitorFitResult(stream);
//	reader_fit.setMonitor(monitor_result);
//
//	msrun->readSpectrum(reader_fit);
//	stream.flush();
//	output.close();
//	delete (msrun);
//	delete (monitor_result);
//}

void GpfEngine::readGpfXmlFile(const QFileInfo &fileinfo) {
	//validation
	this->validateXmlFile(fileinfo);

	GpfXmlParser * parser = new GpfXmlParser();

	QXmlSimpleReader simplereader;
	simplereader.setContentHandler(parser);
	simplereader.setErrorHandler(parser);

	qDebug() << "Read GPF input file '" << fileinfo.filePath() << "'";

	QFile qfile(fileinfo.absoluteFilePath());
	QXmlInputSource xmlInputSource(&qfile);

	if (simplereader.parse(xmlInputSource)) {
	} else {
		throw gpfError(
				QObject::tr("error reading GPF input file :\n").append(
						parser->errorString()));
	}
	qfile.close();
	delete(parser);
	qDebug() << "GPF as finish! Enjoy...";
}


void GpfEngine::validateXmlFile(const QFileInfo &fileinfo) {
	QFile schema_file(GPF_XSD);

	if ( !QFile::exists(GPF_XSD))
	{
		QString schema_sources(GPF_XSD_LOCATION_DIR);
		schema_sources.append("/").append(GPF_SCHEMA_FILE);
		if (QFile::exists(schema_sources))
		{
			schema_file.setFileName(schema_sources);
		} else
		{
			//QString schema_current(_GPF_dir_path);
			//schema_current.append("/").append(GPF_SCHEMA_FILE);*
			QString schema_current(GPF_SCHEMA_FILE);
			if (QFile::exists(schema_current))
			{
				schema_file.setFileName(schema_current);
			} else
			{
				qDebug() << "GPF XSD schema file '%1' does not exist.";
				throw gpfError(QObject::tr("GPF XSD schema is not located in '%1', in '%2' or in '%3'.\n").arg(schema_file.fileName(), schema_sources, schema_current));
			}
		}
	}
	if (!schema_file.open(QIODevice::ReadOnly))
	{
		throw
			gpfError(QObject::tr("error opening the file of the XSD schema : %1 \n Try make install to install the schema file. \n").arg(schema_file.fileName()));
	}
	qDebug() << "GPF::validateXmlFile enter >= 4.6";
	QXmlSchema schema;
	MessageHandler messHandler;
	schema.setMessageHandler(&messHandler);
	/// load verifies if the schema is valid before loading it
	// attention : if schema is invalid the behaviour is undefined (Qt)
	if ( schema.load(&schema_file, QUrl::fromLocalFile(schema_file.fileName())) )
	{
		QFile file(fileinfo.filePath());
		file.open(QIODevice::ReadOnly);
		QXmlSchemaValidator validator(schema);
		if (validator.validate(&file, QUrl::fromLocalFile(file.fileName())))
		{
			qDebug() << "GPF::validateXmlFile instance document is valid";
			file.close();
			schema_file.close();
		}
		else
		{
			qDebug() << "GPF::validateXmlFile instance document is invalid";
			QString err_text(messHandler.description());
			QString err_line, err_column;
			err_line.setNum(messHandler.line());
			err_column.setNum(messHandler.column());
			file.close();
			schema_file.close();
			throw
				gpfError(QObject::tr("error validating against XSD schema in file %1\n at line %2, column %3 : %4\n").arg(fileinfo.filePath(), err_line, err_column, err_text));
		}
	}
	else
	{
		schema_file.close();
		throw
			gpfError(QObject::tr("error validating XSD schema : %1 : is invalid \n").arg(schema_file.fileName()));
	}
}
