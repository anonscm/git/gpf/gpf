/*
 * spectrum_fit_algo.h
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#ifndef SPECTRUM_FIT_ALGO_H_
#define SPECTRUM_FIT_ALGO_H_

#include "../spectrum.h"
#include "spectrum_fit_result.h"
#include <vector>
#include <map>

class SpectrumFitAlgo {
public:
	SpectrumFitAlgo(const GpfParams &gpf_params);
	virtual ~SpectrumFitAlgo();

	virtual void compareSpectrum2Spectrum(const Spectrum & ref,
			const Spectrum & toCompare, SpectrumFitResult &toFill) const=0;

protected:
	void getIntensityMatchedPairs(const Spectrum & ref,
			const Spectrum & toCompare,
			vector<pair<mcq_double, mcq_double> >& toFill) const;
	const mcq_double _ppm_precision;

};

#endif /* SPECTRUM_FIT_ALGO_H_ */
