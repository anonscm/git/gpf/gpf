/*
 * spectrum_reader_collection.cpp
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#include "spectrum_reader_collection.h"
#include <QDebug>
#include <QString>

SpectrumReaderCollection::SpectrumReaderCollection(const GpfParams &gpf_params) :
		SpectrumReaderFilter(gpf_params), SpectrumCollection() {
}

SpectrumReaderCollection::~SpectrumReaderCollection() {
}

void SpectrumReaderCollection::addSpectrumId(SpectrumId &spectrum_id) {
	_spectrum_id_list.insert(spectrum_id);
}

void SpectrumReaderCollection::spectrumEvent(const Spectrum & spectrum) {
	set<SpectrumId>::const_iterator it ;
	if ((it =_spectrum_id_list.find(spectrum.getSpectrumId())
			)!= _spectrum_id_list.end()) {
		qDebug() << "spectrum is collected scan="
				<< spectrum.getSpectrumId().getScan() << " in "
				<< spectrum.getSpectrumId().getMsrun()->getFileName();
		Spectrum light;
		this->filterSpectrum(light, spectrum);
		light.setSpectrumId(*it);
		this->push_back(light);

		//debugging to see if it work
		//light.debugPrintValues();
	}
}
