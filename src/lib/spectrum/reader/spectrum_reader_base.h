/*
 * spectrum_reader_base.h
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#ifndef SPECTRUM_READER_BASE_H_
#define SPECTRUM_READER_BASE_H_

#include "../spectrum.h"
#include "../fit/spectrum_result_base.h"
#include "../../monitor/monitor_base.h"

class SpectrumReaderBase {
public:
	SpectrumReaderBase();
	virtual ~SpectrumReaderBase();

	virtual void spectrumEvent(const Spectrum & spectrum)=0;

	MonitorBase* getMonitor() const {
		return _p_monitor;
	}

	void setMonitor(MonitorBase* monitor) {
		_p_monitor = monitor;
	}

	void setMsRun(MsRun* msrun) {
		_p_msrun = msrun;
	}

	MsRun * getMsRun() const {
		return _p_msrun;
	}
protected:
	MsRun * _p_msrun;
	MonitorBase * _p_monitor;
};

#endif /* SPECTRUM_READER_BASE_H_ */
