/*
 * monitor_base.h
 *
 *  Created on: 18 avr. 2012
 *      Author: valot
 */

#ifndef MONITOR_BASE_H_
#define MONITOR_BASE_H_

#include "../spectrum/spectrum.h"
#include "../spectrum/fit/spectrum_result_base.h"

class MonitorBase {
public:
	MonitorBase();
	virtual ~MonitorBase();

	virtual void currentMatchSpectrum(const Spectrum & match_spectrum)=0;

	virtual void currentReferenceSpectrum(const Spectrum & ref_spectrum)=0;

	virtual void addResult(const SpectrumResultBase & result)=0;
protected:
	Spectrum _match_spectrum;
	Spectrum _ref_spectrum;
};

#endif /* MONITOR_BASE_H_ */
