/*
 * spectrum_fit_algo_cosinus_correlation.h
 *
 *  Created on: 18 avr. 2012
 *      Author: valot
 */

#ifndef SPECTRUM_FIT_ALGO_COSINUS_CORRELATION_H_
#define SPECTRUM_FIT_ALGO_COSINUS_CORRELATION_H_

#include "spectrum_fit_algo.h"
#include "spectrum_fit_algo.h"
#include "spectrum_fit_algo_cosinus_alpha.h"
#include "spectrum_fit_algo_correlation.h"

class SpectrumFitAlgoCosinusCorrelation: public SpectrumFitAlgo {
public:
	SpectrumFitAlgoCosinusCorrelation(const GpfParams &gpf_params);
	virtual ~SpectrumFitAlgoCosinusCorrelation();

	void compareSpectrum2Spectrum(const Spectrum & ref,
			const Spectrum & toCompare, SpectrumFitResult &toFill) const;

private:
	SpectrumFitAlgoCorrelation _algo_correlation;
	SpectrumFitAlgoCosinusAlpha _algo_cosinus;
};

#endif /* SPECTRUM_FIT_ALGO_COSINUS_CORRELATION_H_ */
