/*
 * spectrum_reader_fit.cpp
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#include "spectrum_reader_fit.h"
#include "../spectrum_collection.h"
#include "../fit/spectrum_fit_result.h"

SpectrumReaderFit::SpectrumReaderFit(const GpfParams &gpf_params,
		const SpectrumCollection &spectrum_collection) :
		SpectrumReaderFilter(gpf_params), _spectrum_collection(
				spectrum_collection), _algo_fit(gpf_params) {
}

SpectrumReaderFit::~SpectrumReaderFit() {
	// TODO Auto-generated destructor stub
}

void SpectrumReaderFit::spectrumEvent(const Spectrum &spectrum) {

	//Filter spectrum to Compare
	SpectrumCollection::const_iterator it;
	Spectrum light;
	this->filterSpectrum(light, spectrum);

	//Performed Cosinus calculation
	for (it = _spectrum_collection.begin(); it != _spectrum_collection.end();
			++it) {
		SpectrumFitResult result;
		const Spectrum ref = *it;

		_algo_fit.compareSpectrum2Spectrum(ref, light, result);

		//If result is constent, performed correlation calculation and print
		if (result.isConsistent()) {
			if (_p_monitor != NULL) {
				_p_monitor->currentMatchSpectrum(light);
				_p_monitor->currentReferenceSpectrum(ref);
				_p_monitor->addResult(result);
			}else{
			qDebug() << "Matched Scan ref=" << ref.getSpectrumId().getScan()
					<< "\t" << spectrum.getSpectrumId().getScan();
			}
			//TODO push result on monitor
		} else {
		}
	}
}
