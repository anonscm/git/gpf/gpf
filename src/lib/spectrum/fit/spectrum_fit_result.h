/*
 * spectrum_fit_result.h
 *
 *  Created on: 11 avr. 2012
 *      Author: langella
 */

#ifndef SPECTRUM_FIT_RESULT_H_
#define SPECTRUM_FIT_RESULT_H_

#include "../../gpf_types.h"
#include "../../gpf_params.h"
#include "../../gpf_error.h"
#include "spectrum_result_base.h"
#include <QObject>
#include <iostream>

using namespace std;

class SpectrumFitResult : public SpectrumResultBase {
public:
	SpectrumFitResult();
	virtual ~SpectrumFitResult();

	void getSpectrumResultMap(std::map<QString,QString> &toFill) const;

	mcq_double getCorrelationCoefficient() const {
		return _correlation_coefficient;
	}

	void setCorrelationCoefficient(mcq_double correlationCoefficient) {
		_correlation_coefficient = correlationCoefficient;
	}

	mcq_double getCosinusScore() const {
		return _cosinus_score;
	}

	void setCosinusScore(mcq_double cosinusScore) {
		_cosinus_score = cosinusScore;
	}

	mcq_double getPearsonProbability() const {
		return _pearson_probability;
	}

	void setPearsonProbability(mcq_double pearsonProbability) {
		_pearson_probability = pearsonProbability;
	}

	int getPeakCount() const {
		return _peak_count;
	}

	void setPeakCount(int peakCount) {
		_peak_count = peakCount;
	}

	bool isConsistent() const {
		return(_is_consitent);
	}

	void setConsistent(bool consistent){
		_is_consitent = consistent;
	}

private :
	int _peak_count;
	mcq_double _cosinus_score;
	mcq_double _correlation_coefficient;
	mcq_double _pearson_probability;
	bool _is_consitent;
};

#endif /* SPECTRUM_FIT_RESULT_H_ */
