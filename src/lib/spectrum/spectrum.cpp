/**
 * \file spectrum.cpp
 * \date 18 janv. 2010
 * \author Olivier Langella
 */

#include "spectrum.h"
#include "../gpf_error.h"

#include <math.h>
#include <limits>
#include <vector>
#include <QDebug>
#include <map>

Spectrum::Spectrum() :
	_spectrum_id(0,NULL) {
	_v_peaks.resize(0);
	_precursor_mz = -1;
	_precursor_z = -1;
	_precursor_int = 0;
}

Spectrum::Spectrum(const SpectrumId & spectrum_id) :
		_spectrum_id(spectrum_id) {
	_v_peaks.resize(0);
	_precursor_mz = -1;
	_precursor_z = -1;
	_precursor_int = 0;
}

Spectrum::Spectrum(const Spectrum & toCopy) :
		_v_peaks(toCopy._v_peaks), _spectrum_id(toCopy._spectrum_id) {
	copyMembers(toCopy);
}

Spectrum::~Spectrum() {
	_v_peaks.clear();
}

void Spectrum::debugPrintValues() const {
	std::vector<mz_int>::const_iterator it(_v_peaks.begin());
	for (; it != _v_peaks.end(); it++) {
		qDebug() << "mz = " << it->first << ", int = " << it->second;
	}
}

const unsigned int Spectrum::getSpectrumSize() const {
	return (_v_peaks.size());
}

void Spectrum::clear() {
	_v_peaks.clear();
	_precursor_mz = -1;
	_precursor_z = -1;
	_precursor_int = 0;
}

void Spectrum::reserve(const unsigned int size) {
	_v_peaks.reserve(size);
}

mcq_double Spectrum::getPrecursorMh() const{
	mcq_double mh = _precursor_mz * ((mcq_double) _precursor_z);
	mh = mh - (MHPLUS * ((mcq_double) _precursor_z-1));
	return(mh);
}


mcq_double Spectrum::getMaxIntensity() const {
	//mcq_double max(numeric_limits<mcq_double>::quiet_NaN());
	mcq_double max(0);
	std::vector<mz_int>::const_iterator it(_v_peaks.begin());
	for (; it != _v_peaks.end(); it++) {
		if (max < it->second) {
			max = it->second;
		}
	}
	return (max);
}

void Spectrum::push_back(const mz_int& mz_int_pair) {
	_v_peaks.push_back(mz_int_pair);
}

/** @brief take n most intense peaks from the spectrum to copy
 *
 * @param n
 * @param toCopy
 */
void Spectrum::takeNmostIntense(int n, const Spectrum & toCopy) {

	_v_peaks.clear();

	multimap<mcq_double, mcq_double, greater<mcq_double> > sorted_spectrum_int2mz;

	std::vector<mz_int>::const_iterator itSpectrum(toCopy._v_peaks.begin());
	for (; itSpectrum != toCopy._v_peaks.end(); itSpectrum++) {
		sorted_spectrum_int2mz.insert(
				pair<mcq_double, mcq_double>(itSpectrum->second,
						itSpectrum->first));
	}

	copyMembers(toCopy);

	multimap<mcq_double, mcq_double>::const_iterator it(
			sorted_spectrum_int2mz.begin());

	map<mcq_double, mcq_double> sorted_spectrum_mz2int;
	for (int i = 0; ((i < n) && (it != sorted_spectrum_int2mz.end()));
			i++, it++) {
		sorted_spectrum_mz2int.insert(
				pair<mcq_double, mcq_double>(it->second, it->first));
	}

	map<mcq_double, mcq_double>::const_iterator itMz2Int(
			sorted_spectrum_mz2int.begin());
	_v_peaks.reserve(sorted_spectrum_mz2int.size());
	for (; itMz2Int != sorted_spectrum_mz2int.end(); itMz2Int++) {
		_v_peaks.push_back(*itMz2Int);
	}

}

void Spectrum::applyCutOff(mcq_double mz_min, const Spectrum & toCopy) {
	_v_peaks.clear();
	_v_peaks.reserve(toCopy._v_peaks.size());
	copyMembers(toCopy);
	std::vector<mz_int>::const_iterator it(toCopy._v_peaks.begin());
	for (; it != toCopy._v_peaks.end(); it++) {
		if (mz_min < it->first) {
			_v_peaks.push_back(*it);
		}
	}

}
void Spectrum::cleanPeakIntensity(float percentIntMIn,
		const Spectrum & toCopy) {
	_v_peaks.clear();
	_v_peaks.reserve(toCopy._v_peaks.size());
	copyMembers(toCopy);

	mcq_double max = this->getMaxIntensity();
	mcq_double intCutOff = max * (mcq_double) percentIntMIn;

	std::vector<mz_int>::const_iterator it(toCopy._v_peaks.begin());
	for (; it != toCopy._v_peaks.end(); it++) {
		if (intCutOff < it->second) {
			_v_peaks.push_back(*it);
		}
	}

}

void Spectrum::copyMembers(const Spectrum & toCopy) {
	_precursor_mz = toCopy._precursor_mz;
	_precursor_z = toCopy._precursor_z;
	_precursor_int = toCopy._precursor_int;
	_spectrum_id = toCopy._spectrum_id;
}

