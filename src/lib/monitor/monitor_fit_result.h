/*
 * monitor_fit_result.h
 *
 *  Created on: 19 avr. 2012
 *      Author: valot
 */

#ifndef MONITOR_FIT_RESULT_H_
#define MONITOR_FIT_RESULT_H_

#include "monitor_base.h"
#include <QTextStream>
#include <QString>
#include <map>
#include <QStringList>

class MonitorFitResult: public MonitorBase {
public:
	MonitorFitResult(QTextStream &output_stream);
	virtual ~MonitorFitResult();

	void currentMatchSpectrum(const Spectrum & match_spectrum);

	void currentReferenceSpectrum(const Spectrum & ref_spectrum);

	void addResult(const SpectrumResultBase & result);

private:
	void printHeaders(const std::map<QString, QString> &pairResult);

	void printLine(const QStringList & list);

	bool _print_header;
	QTextStream &_output_stream;
	const QString _sep;
};

#endif /* MONITOR_FIT_RESULT_H_ */
