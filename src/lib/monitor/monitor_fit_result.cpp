/*
 * monitor_fit_result.cpp
 *
 *  Created on: 19 avr. 2012
 *      Author: valot
 */

#include "monitor_fit_result.h"
#include "../gpf_error.h"
#include <QObject>
#include <QDebug>

MonitorFitResult::MonitorFitResult(QTextStream &output_stream) :
	_output_stream(output_stream), _sep("\t")
{
	//initialise variable
	_print_header = true;
}

MonitorFitResult::~MonitorFitResult() {
	_output_stream.flush();
}

void MonitorFitResult::currentMatchSpectrum(const Spectrum & match_spectrum) {
	_match_spectrum = match_spectrum;
}

void MonitorFitResult::currentReferenceSpectrum(const Spectrum & ref_spectrum) {
	_ref_spectrum = ref_spectrum;
}

void MonitorFitResult::addResult(const SpectrumResultBase & result) {
	//SPectrum are complete?
	if (_match_spectrum.getSpectrumId().getScan() == 0){
		throw gpfError(QObject::tr(	"Matched Spectrum is empty for monitoring \n"));
	}else if (_ref_spectrum.getSpectrumId().getScan() == 0){
		throw gpfError(QObject::tr(	"Reference Spectrum is empty for monitoring \n"));
	}

	//results
	std::map<QString, QString> pairResult;
	result.getSpectrumResultMap(pairResult);

	//first printing?
	if(_print_header){
		this->printHeaders(pairResult);
		_print_header=false;
	}

	//Now, printing
	QStringList list;
	//spectrum Information
	list.append(_match_spectrum.getSpectrumId().getMsrun()->getFileName());
	list.append(QString().setNum(_match_spectrum.getSpectrumId().getScan(),10));
	list.append(QString().setNum(_match_spectrum.getPrecursorMh(),'g',8));
	list.append(QString().setNum(_match_spectrum.getPrecursorZ(),10));
	list.append(_ref_spectrum.getSpectrumId().getMsrun()->getFileName());
	list.append(QString().setNum(_ref_spectrum.getSpectrumId().getScan(),10));
	list.append(_ref_spectrum.getSpectrumId().getSpectrumInfo());
	list.append(QString().setNum(_ref_spectrum.getPrecursorMh(),'g',8));
	list.append(QString().setNum(_ref_spectrum.getPrecursorZ(),10));
	mcq_double delta = _match_spectrum.getPrecursorMh() - _ref_spectrum.getPrecursorMh();
	list.append(QString().setNum(delta,'g',8));
	std::map<QString, QString>::const_iterator itresult;
	for(itresult=pairResult.begin();itresult!=pairResult.end();itresult++){
		list.append(itresult->second);
	}
	this->printLine(list);
}

void MonitorFitResult::printHeaders(const std::map<QString, QString> &pairResult) {
	QStringList list;
	list.append("MsRun Match");
	list.append("Scan Match");
	list.append("Mh+ Match");
	list.append("z Match");
	list.append("MsRun Ref");
	list.append("Scan Ref");
	list.append("Info Ref");
	list.append("Mh+ Ref");
	list.append("z Ref");
	list.append("deltaMh+");
	std::map<QString, QString>::const_iterator itresult;
	for(itresult=pairResult.begin();itresult!=pairResult.end();itresult++){
		list.append(itresult->first);
	}
	this->printLine(list);
}

void MonitorFitResult::printLine(const QStringList & list)
{
	_output_stream << list.join(_sep) << endl;
}

