/*
 * spectrum_id.h
 *
 *  Created on: 14 avr. 2012
 *      Author: olivier
 */

#ifndef SPECTRUM_ID_H_
#define SPECTRUM_ID_H_

#include <QString>
#include <QDebug>
#include "ms_run.h"

class SpectrumId {
public:
	SpectrumId(int scan_num, MsRun* msrun);
	SpectrumId(const SpectrumId & toCopy);
	virtual ~SpectrumId();

	void setSpectrumInfo(const QString &spectrumInfo) {
		_spectrum_info = spectrumInfo;
	}

	MsRun * getMsrun() const {
		return _p_msrun;
	}

	QString getSpectrumInfo() const {
		return _spectrum_info;
	}

	int getScan() const {
		return _scan;
	}

	bool operator <(const SpectrumId& id) const {
		if (*_p_msrun == *id._p_msrun) {
			if (_scan < id._scan) {
				return (true);
			} else {
				return (false);
			}
		} else {
			return (*_p_msrun < *id._p_msrun);
		}
	}

private:

	int _scan;

	MsRun* _p_msrun;

	QString _spectrum_info;

};

#endif /* SPECTRUM_ID_H_ */
