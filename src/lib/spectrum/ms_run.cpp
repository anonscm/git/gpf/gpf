/*
 * ms_run.cpp
 *
 *  Created on: 17 avr. 2012
 *      Author: valot
 */

#include "ms_run.h"
#include "../gpf_error.h"
#include "reader/spectrum_reader_base.h"
#include "../saxparsers/mzxmlSpectrumParser.h"
#include <QXmlInputSource>
#include <QFile>

MsRun::MsRun(const QFileInfo &fileinf):
_fileinf(fileinf)
{
}

MsRun::~MsRun() {
	// TODO Auto-generated destructor stub
}

void MsRun::readSpectrum(SpectrumReaderBase &reader) {
	//set MsRun to reader
	reader.setMsRun(this);

	MzxmlSpectrumParser parser(reader);

	QXmlSimpleReader simplereader;
	simplereader.setContentHandler(&parser);
	simplereader.setErrorHandler(&parser);

	qDebug() << "Read mzXml input file '" << _fileinf.filePath() << "'";

	QFile qfile(_fileinf.absoluteFilePath());
	QXmlInputSource xmlInputSource(&qfile);

	if (simplereader.parse(xmlInputSource)) {
	} else {
		throw gpfError(
				QObject::tr("error reading mzXml input file :\n").append(
						parser.errorString()));
	}
	qfile.close();
	qDebug() << "gpf : DONE on file '" << _fileinf.filePath() << "'";

}
