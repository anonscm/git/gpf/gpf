/*
 * spectrum_result_base.h
 *
 *  Created on: 18 avr. 2012
 *      Author: valot
 */

#ifndef SPECTRUM_RESULT_BASE_H_
#define SPECTRUM_RESULT_BASE_H_

#include <map>
#include <QString>

class SpectrumResultBase {
public:
	SpectrumResultBase();
	virtual ~SpectrumResultBase();
	virtual void getSpectrumResultMap(std::map<QString,QString> &toFill) const=0;
};

#endif /* SPECTRUM_RESULT_BASE_H_ */
