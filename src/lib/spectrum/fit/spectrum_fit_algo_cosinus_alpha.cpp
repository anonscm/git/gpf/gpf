/*
 * spectrum_fit_algo_cosinus_alpha.cpp
 *
 *  Created on: 17 avr. 2012
 *      Author: valot
 */

#include "spectrum_fit_algo_cosinus_alpha.h"

SpectrumFitAlgoCosinusAlpha::SpectrumFitAlgoCosinusAlpha(const GpfParams &gpf_params) :
		SpectrumFitAlgo(gpf_params),
		_min_number_of_peak_to_score(gpf_params.getMinNumberOfPeakToScore()),
		_min_cosinus_limit(gpf_params.getMinCosinus())
{
}

SpectrumFitAlgoCosinusAlpha::~SpectrumFitAlgoCosinusAlpha() {
}

void SpectrumFitAlgoCosinusAlpha::compareSpectrum2Spectrum(const Spectrum & ref,
		const Spectrum & toCompare, SpectrumFitResult &toFill) const {

	//get matched peak
	vector<pair<mcq_double, mcq_double> > matched_int;
	this->getIntensityMatchedPairs(ref, toCompare, matched_int);

	toFill.setPeakCount(matched_int.size());
	toFill.setConsistent(false);

	//If peak number is correct and consinus > limit, result is consistant
	if (toFill.getPeakCount() > _min_number_of_peak_to_score) {
		//Calculated cosinus alpha
		mcq_double consinus_alpha = this->calculated_inner_product(matched_int);
		consinus_alpha = consinus_alpha
				/ (this->calculated_length_vector(ref)
						* this->calculated_length_vector(toCompare));
		toFill.setCosinusScore(consinus_alpha);

		if(consinus_alpha>_min_cosinus_limit){
			toFill.setConsistent(true);
		}
	}
}

mcq_double SpectrumFitAlgoCosinusAlpha::calculated_inner_product(
		std::vector<pair<mcq_double, mcq_double> > &matched_int) const {
	//somme des produits des pics communs

	mcq_double inner(0);
	std::vector<pair<mcq_double, mcq_double> >::const_iterator map_iterated(
			matched_int.begin());
	for (; map_iterated != matched_int.end(); map_iterated++) {
		inner += (map_iterated->first * map_iterated->second);
	}
	return inner;
}

mcq_double SpectrumFitAlgoCosinusAlpha::calculated_length_vector(
		const Spectrum & spectrum) const {
	//racine carré de la somme des carrés de chaque pic
	mcq_double vector_length(0);
	Spectrum::const_iterator peaks(spectrum.begin());
	for (; peaks != spectrum.end(); peaks++) {
		vector_length += (peaks->second * peaks->second);
	}
	vector_length = sqrt(vector_length);
	return (vector_length);
}

