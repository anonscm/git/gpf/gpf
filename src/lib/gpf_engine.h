/*
 * gpf_engine.h
 *
 *  Created on: 10 avr. 2012
 *      Author: valot
 */

#ifndef GPF_ENGINE_H_
#define GPF_ENGINE_H_

#include "spectrum/spectrum.h"
#include "gpf_params.h"
#include "gpf_types.h"
#include <QFileInfo>


class GpfEngine {
public:
	GpfEngine();
	virtual ~GpfEngine();
	void readGpfXmlFile(const QFileInfo &fileinfo);
private:
	void validateXmlFile(const QFileInfo &fileinfo);
	GpfParams _gpf_params;
};

#endif /* GPF_ENGINE_H_ */
